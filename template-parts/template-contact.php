<?php
/**
* Template Name: Contact Page
*
* @package WordPress
* @subpackage theme_by_socreativ
* @since theme_by_socreativ 1.0
*/


get_header(null, array('css' => array('sticky-locked', 'no-speedtrap')));
?>

	<main id="primary" class="site-main">

        <div class="contact-container d-flex">

            <div class="map" style-location="<?= get_stylesheet_directory_uri() . '/assets/js/mapStyle.js' ?>">

            </div>

            <div class="contact">
               <?php the_content() ?>
            </div>

        </div>

	</main><!-- #main -->

<?php
get_footer();
