<div class="top-right corner">
    <svg width="22" height="11" viewBox="0 0 22 11" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0.75 1L20.75 1L20.75 11" stroke="white" stroke-width="1.25"/>
    </svg>
</div>

<div class="central-player hover-glitch-container">
    <?php if(wp_is_mobile()){ ?>
        <svg width="40" height="46" viewBox="0 0 40 46" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M38.5 21.2679C39.8333 22.0377 39.8333 23.9623 38.5 24.7321L3.25 45.0836C1.91667 45.8534 0.249998 44.8912 0.249998 43.3516L0.25 2.6484C0.25 1.1088 1.91667 0.14655 3.25 0.91635L38.5 21.2679Z" fill="white"/>
        </svg>
    <?php }else{ ?>
        <div class="glitch-container hover-glitch">
            <svg width="40" height="46" viewBox="0 0 40 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M38.5 21.2679C39.8333 22.0377 39.8333 23.9623 38.5 24.7321L3.25 45.0836C1.91667 45.8534 0.249998 44.8912 0.249998 43.3516L0.25 2.6484C0.25 1.1088 1.91667 0.14655 3.25 0.91635L38.5 21.2679Z" fill="white"/>
            </svg>
            <svg width="40" height="46" viewBox="0 0 40 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M38.5 21.2679C39.8333 22.0377 39.8333 23.9623 38.5 24.7321L3.25 45.0836C1.91667 45.8534 0.249998 44.8912 0.249998 43.3516L0.25 2.6484C0.25 1.1088 1.91667 0.14655 3.25 0.91635L38.5 21.2679Z" fill="white"/>
            </svg>
            <svg width="40" height="46" viewBox="0 0 40 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M38.5 21.2679C39.8333 22.0377 39.8333 23.9623 38.5 24.7321L3.25 45.0836C1.91667 45.8534 0.249998 44.8912 0.249998 43.3516L0.25 2.6484C0.25 1.1088 1.91667 0.14655 3.25 0.91635L38.5 21.2679Z" fill="white"/>
            </svg>
        </div>
    <?php } ?>
</div>

<div class="bottom-left corner">
    <svg width="22" height="11" viewBox="0 0 22 11" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M21.25 10L1.25 10L1.25 -3.02996e-06" stroke="white" stroke-width="1.25"/>
    </svg>
</div>