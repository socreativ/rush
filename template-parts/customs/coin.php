<div class="coin">

    <div class="sprite sprite-0 current">
        <svg width="160" height="280" viewBox="0 0 160 280" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect x="80" width="20" height="20" transform="rotate(90 80 0)" fill="#FC9838"/>
            <rect x="100" width="20" height="20" transform="rotate(90 100 0)" fill="#FC9838"/>
            <rect x="80" y="160" width="20" height="20" transform="rotate(90 80 160)" fill="#FC9838"/>
            <rect x="100" y="160" width="20" height="20" transform="rotate(90 100 160)" fill="#FC9838"/>
            <rect x="120" y="160" width="20" height="20" transform="rotate(90 120 160)" fill="#D82800"/>
            <rect x="140" y="160" width="20" height="20" transform="rotate(90 140 160)" fill="#FC9838"/>
            <rect x="160" y="160" width="20" height="20" transform="rotate(90 160 160)" fill="#FC9838"/>
            <rect x="60" y="160" width="20" height="20" transform="rotate(90 60 160)" fill="white"/>
            <rect x="40" y="160" width="20" height="20" transform="rotate(90 40 160)" fill="#FC9838"/>
            <rect x="20" y="160" width="20" height="20" transform="rotate(90 20 160)" fill="#FC9838"/>
            <rect x="80" y="100" width="20" height="20" transform="rotate(90 80 100)" fill="#FC9838"/>
            <rect x="100" y="100" width="20" height="20" transform="rotate(90 100 100)" fill="#FC9838"/>
            <rect x="120" y="100" width="20" height="20" transform="rotate(90 120 100)" fill="#D82800"/>
            <rect x="140" y="100" width="20" height="20" transform="rotate(90 140 100)" fill="#FC9838"/>
            <rect x="160" y="100" width="20" height="20" transform="rotate(90 160 100)" fill="#FC9838"/>
            <rect x="60" y="100" width="20" height="20" transform="rotate(90 60 100)" fill="white"/>
            <rect x="40" y="100" width="20" height="20" transform="rotate(90 40 100)" fill="#FC9838"/>
            <rect x="20" y="100" width="20" height="20" transform="rotate(90 20 100)" fill="#FC9838"/>
            <rect x="80" y="40" width="20" height="20" transform="rotate(90 80 40)" fill="#FC9838"/>
            <rect x="100" y="40" width="20" height="20" transform="rotate(90 100 40)" fill="#FC9838"/>
            <rect x="120" y="40" width="20" height="20" transform="rotate(90 120 40)" fill="#FC9838"/>
            <rect x="140" y="40" width="20" height="20" transform="rotate(90 140 40)" fill="#FC9838"/>
            <rect x="120" y="20" width="20" height="20" transform="rotate(90 120 20)" fill="#FC9838"/>
            <rect x="60" y="40" width="20" height="20" transform="rotate(90 60 40)" fill="#FC9838"/>
            <rect x="40" y="40" width="20" height="20" transform="rotate(90 40 40)" fill="#FC9838"/>
            <rect x="60" y="20" width="20" height="20" transform="rotate(90 60 20)" fill="#FC9838"/>
            <rect x="80" y="140" width="20" height="20" transform="rotate(90 80 140)" fill="#FC9838"/>
            <rect x="100" y="140" width="20" height="20" transform="rotate(90 100 140)" fill="#FC9838"/>
            <rect x="120" y="140" width="20" height="20" transform="rotate(90 120 140)" fill="#D82800"/>
            <rect x="140" y="140" width="20" height="20" transform="rotate(90 140 140)" fill="#FC9838"/>
            <rect x="160" y="140" width="20" height="20" transform="rotate(90 160 140)" fill="#FC9838"/>
            <rect x="60" y="140" width="20" height="20" transform="rotate(90 60 140)" fill="white"/>
            <rect x="40" y="140" width="20" height="20" transform="rotate(90 40 140)" fill="#FC9838"/>
            <rect x="20" y="140" width="20" height="20" transform="rotate(90 20 140)" fill="#FC9838"/>
            <rect x="80" y="20" width="20" height="20" transform="rotate(90 80 20)" fill="#FC9838"/>
            <rect x="100" y="20" width="20" height="20" transform="rotate(90 100 20)" fill="#FC9838"/>
            <rect x="80" y="180" width="20" height="20" transform="rotate(90 80 180)" fill="#FC9838"/>
            <rect x="100" y="180" width="20" height="20" transform="rotate(90 100 180)" fill="#FC9838"/>
            <rect x="120" y="180" width="20" height="20" transform="rotate(90 120 180)" fill="#D82800"/>
            <rect x="140" y="180" width="20" height="20" transform="rotate(90 140 180)" fill="#FC9838"/>
            <rect x="160" y="180" width="20" height="20" transform="rotate(90 160 180)" fill="#FC9838"/>
            <rect x="60" y="180" width="20" height="20" transform="rotate(90 60 180)" fill="white"/>
            <rect x="40" y="180" width="20" height="20" transform="rotate(90 40 180)" fill="#FC9838"/>
            <rect x="20" y="180" width="20" height="20" transform="rotate(90 20 180)" fill="#FC9838"/>
            <rect x="80" y="120" width="20" height="20" transform="rotate(90 80 120)" fill="#FC9838"/>
            <rect x="100" y="120" width="20" height="20" transform="rotate(90 100 120)" fill="#FC9838"/>
            <rect x="120" y="120" width="20" height="20" transform="rotate(90 120 120)" fill="#D82800"/>
            <rect x="140" y="120" width="20" height="20" transform="rotate(90 140 120)" fill="#FC9838"/>
            <rect x="160" y="120" width="20" height="20" transform="rotate(90 160 120)" fill="#FC9838"/>
            <rect x="60" y="120" width="20" height="20" transform="rotate(90 60 120)" fill="white"/>
            <rect x="40" y="120" width="20" height="20" transform="rotate(90 40 120)" fill="#FC9838"/>
            <rect x="20" y="120" width="20" height="20" transform="rotate(90 20 120)" fill="#FC9838"/>
            <rect x="80" y="60" width="20" height="20" transform="rotate(90 80 60)" fill="white"/>
            <rect x="100" y="60" width="20" height="20" transform="rotate(90 100 60)" fill="#D82800"/>
            <rect x="120" y="60" width="20" height="20" transform="rotate(90 120 60)" fill="#FC9838"/>
            <rect x="140" y="60" width="20" height="20" transform="rotate(90 140 60)" fill="#FC9838"/>
            <rect x="60" y="60" width="20" height="20" transform="rotate(90 60 60)" fill="#FC9838"/>
            <rect x="40" y="60" width="20" height="20" transform="rotate(90 40 60)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 80 280)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 100 280)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 80 240)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 100 240)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 120 240)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 140 240)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 120 260)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 60 240)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 40 240)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 60 260)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 80 260)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 100 260)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 80 220)" fill="white"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 100 220)" fill="#D82800"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 120 220)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 140 220)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 60 220)" fill="#FC9838"/>
            <rect width="20" height="20" transform="matrix(-4.37114e-08 -1 -1 4.37114e-08 40 220)" fill="#FC9838"/>
            <rect x="80" y="80" width="20" height="20" transform="rotate(90 80 80)" fill="#FC9838"/>
            <rect x="100" y="80" width="20" height="20" transform="rotate(90 100 80)" fill="#FC9838"/>
            <rect x="120" y="80" width="20" height="20" transform="rotate(90 120 80)" fill="#D82800"/>
            <rect x="140" y="80" width="20" height="20" transform="rotate(90 140 80)" fill="#FC9838"/>
            <rect x="160" y="80" width="20" height="20" transform="rotate(90 160 80)" fill="#FC9838"/>
            <rect x="60" y="80" width="20" height="20" transform="rotate(90 60 80)" fill="white"/>
            <rect x="40" y="80" width="20" height="20" transform="rotate(90 40 80)" fill="#FC9838"/>
            <rect x="20" y="80" width="20" height="20" transform="rotate(90 20 80)" fill="#FC9838"/>
        </svg>
    </div>

    <div class="sprite sprite-1">
        <svg width="160" height="280" viewBox="0 0 160 280" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect x="80" width="20" height="20" transform="rotate(90 80 0)" fill="#FC9838"/>
            <rect x="100" width="20" height="20" transform="rotate(90 100 0)" fill="#D82800"/>
            <rect x="80" y="160" width="20" height="20" transform="rotate(90 80 160)" fill="#D82800"/>
            <rect x="100" y="160" width="20" height="20" transform="rotate(90 100 160)" fill="#D82800"/>
            <rect x="120" y="160" width="20" height="20" transform="rotate(90 120 160)" fill="#D82800"/>
            <rect x="60" y="160" width="20" height="20" transform="rotate(90 60 160)" fill="#FC9838"/>
            <rect x="80" y="100" width="20" height="20" transform="rotate(90 80 100)" fill="#D82800"/>
            <rect x="100" y="100" width="20" height="20" transform="rotate(90 100 100)" fill="#D82800"/>
            <rect x="120" y="100" width="20" height="20" transform="rotate(90 120 100)" fill="#D82800"/>
            <rect x="60" y="100" width="20" height="20" transform="rotate(90 60 100)" fill="#FC9838"/>
            <rect x="80" y="260" width="20" height="20" transform="rotate(90 80 260)" fill="#FC9838"/>
            <rect x="100" y="260" width="20" height="20" transform="rotate(90 100 260)" fill="#D82800"/>
            <rect x="80" y="40" width="20" height="20" transform="rotate(90 80 40)" fill="#D82800"/>
            <rect x="100" y="40" width="20" height="20" transform="rotate(90 100 40)" fill="#D82800"/>
            <rect x="120" y="40" width="20" height="20" transform="rotate(90 120 40)" fill="#D82800"/>
            <rect x="60" y="40" width="20" height="20" transform="rotate(90 60 40)" fill="#FC9838"/>
            <rect x="80" y="200" width="20" height="20" transform="rotate(90 80 200)" fill="#D82800"/>
            <rect x="100" y="200" width="20" height="20" transform="rotate(90 100 200)" fill="#D82800"/>
            <rect x="120" y="200" width="20" height="20" transform="rotate(90 120 200)" fill="#D82800"/>
            <rect x="60" y="200" width="20" height="20" transform="rotate(90 60 200)" fill="#FC9838"/>
            <rect x="80" y="140" width="20" height="20" transform="rotate(90 80 140)" fill="#D82800"/>
            <rect x="100" y="140" width="20" height="20" transform="rotate(90 100 140)" fill="#D82800"/>
            <rect x="120" y="140" width="20" height="20" transform="rotate(90 120 140)" fill="#D82800"/>
            <rect x="60" y="140" width="20" height="20" transform="rotate(90 60 140)" fill="white"/>
            <rect x="80" y="20" width="20" height="20" transform="rotate(90 80 20)" fill="#FC9838"/>
            <rect x="100" y="20" width="20" height="20" transform="rotate(90 100 20)" fill="#D82800"/>
            <rect x="80" y="180" width="20" height="20" transform="rotate(90 80 180)" fill="#D82800"/>
            <rect x="100" y="180" width="20" height="20" transform="rotate(90 100 180)" fill="#D82800"/>
            <rect x="120" y="180" width="20" height="20" transform="rotate(90 120 180)" fill="#D82800"/>
            <rect x="60" y="180" width="20" height="20" transform="rotate(90 60 180)" fill="#FC9838"/>
            <rect x="80" y="120" width="20" height="20" transform="rotate(90 80 120)" fill="#D82800"/>
            <rect x="100" y="120" width="20" height="20" transform="rotate(90 100 120)" fill="#D82800"/>
            <rect x="120" y="120" width="20" height="20" transform="rotate(90 120 120)" fill="#D82800"/>
            <rect x="60" y="120" width="20" height="20" transform="rotate(90 60 120)" fill="white"/>
            <rect x="80" y="60" width="20" height="20" transform="rotate(90 80 60)" fill="#D82800"/>
            <rect x="100" y="60" width="20" height="20" transform="rotate(90 100 60)" fill="#D82800"/>
            <rect x="120" y="60" width="20" height="20" transform="rotate(90 120 60)" fill="#D82800"/>
            <rect x="60" y="60" width="20" height="20" transform="rotate(90 60 60)" fill="#FC9838"/>
            <rect x="80" y="220" width="20" height="20" transform="rotate(90 80 220)" fill="#D82800"/>
            <rect x="100" y="220" width="20" height="20" transform="rotate(90 100 220)" fill="#D82800"/>
            <rect x="120" y="220" width="20" height="20" transform="rotate(90 120 220)" fill="#D82800"/>
            <rect x="60" y="220" width="20" height="20" transform="rotate(90 60 220)" fill="#FC9838"/>
            <rect x="80" y="80" width="20" height="20" transform="rotate(90 80 80)" fill="#D82800"/>
            <rect x="100" y="80" width="20" height="20" transform="rotate(90 100 80)" fill="#D82800"/>
            <rect x="120" y="80" width="20" height="20" transform="rotate(90 120 80)" fill="#D82800"/>
            <rect x="60" y="80" width="20" height="20" transform="rotate(90 60 80)" fill="#FC9838"/>
            <rect x="80" y="240" width="20" height="20" transform="rotate(90 80 240)" fill="#FC9838"/>
            <rect x="100" y="240" width="20" height="20" transform="rotate(90 100 240)" fill="#D82800"/>
        </svg>
    </div>


    <div class="sprite sprite-2">
        <svg width="160" height="280" viewBox="0 0 160 280" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect x="90" width="20" height="20" transform="rotate(90 90 0)" fill="#FC9838"/>
            <rect x="90" y="160" width="20" height="20" transform="rotate(90 90 160)" fill="#FC9838"/>
            <rect x="90" y="100" width="20" height="20" transform="rotate(90 90 100)" fill="#FC9838"/>
            <rect x="90" y="260" width="20" height="20" transform="rotate(90 90 260)" fill="#FC9838"/>
            <rect x="90" y="40" width="20" height="20" transform="rotate(90 90 40)" fill="#FC9838"/>
            <rect x="90" y="200" width="20" height="20" transform="rotate(90 90 200)" fill="#FC9838"/>
            <rect x="90" y="140" width="20" height="20" transform="rotate(90 90 140)" fill="white"/>
            <rect x="90" y="20" width="20" height="20" transform="rotate(90 90 20)" fill="#FC9838"/>
            <rect x="90" y="180" width="20" height="20" transform="rotate(90 90 180)" fill="#FC9838"/>
            <rect x="90" y="120" width="20" height="20" transform="rotate(90 90 120)" fill="white"/>
            <rect x="90" y="60" width="20" height="20" transform="rotate(90 90 60)" fill="#FC9838"/>
            <rect x="90" y="220" width="20" height="20" transform="rotate(90 90 220)" fill="#FC9838"/>
            <rect x="90" y="80" width="20" height="20" transform="rotate(90 90 80)" fill="#FC9838"/>
            <rect x="90" y="240" width="20" height="20" transform="rotate(90 90 240)" fill="#FC9838"/>
        </svg>
    </div>

    <div class="sprite sprite-3">
        <svg width="160" height="280" viewBox="0 0 160 280" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect x="80" width="20" height="20" transform="rotate(90 80 0)" fill="white"/>
            <rect x="100" width="20" height="20" transform="rotate(90 100 0)" fill="#D82800"/>
            <rect x="80" y="160" width="20" height="20" transform="rotate(90 80 160)" fill="white"/>
            <rect x="100" y="160" width="20" height="20" transform="rotate(90 100 160)" fill="white"/>
            <rect x="120" y="160" width="20" height="20" transform="rotate(90 120 160)" fill="#D82800"/>
            <rect x="60" y="160" width="20" height="20" transform="rotate(90 60 160)" fill="white"/>
            <rect x="80" y="100" width="20" height="20" transform="rotate(90 80 100)" fill="white"/>
            <rect x="100" y="100" width="20" height="20" transform="rotate(90 100 100)" fill="white"/>
            <rect x="120" y="100" width="20" height="20" transform="rotate(90 120 100)" fill="#D82800"/>
            <rect x="60" y="100" width="20" height="20" transform="rotate(90 60 100)" fill="white"/>
            <rect x="80" y="260" width="20" height="20" transform="rotate(90 80 260)" fill="white"/>
            <rect x="100" y="260" width="20" height="20" transform="rotate(90 100 260)" fill="#D82800"/>
            <rect x="80" y="40" width="20" height="20" transform="rotate(90 80 40)" fill="white"/>
            <rect x="100" y="40" width="20" height="20" transform="rotate(90 100 40)" fill="white"/>
            <rect x="120" y="40" width="20" height="20" transform="rotate(90 120 40)" fill="#D82800"/>
            <rect x="60" y="40" width="20" height="20" transform="rotate(90 60 40)" fill="white"/>
            <rect x="80" y="200" width="20" height="20" transform="rotate(90 80 200)" fill="white"/>
            <rect x="100" y="200" width="20" height="20" transform="rotate(90 100 200)" fill="white"/>
            <rect x="120" y="200" width="20" height="20" transform="rotate(90 120 200)" fill="#D82800"/>
            <rect x="60" y="200" width="20" height="20" transform="rotate(90 60 200)" fill="white"/>
            <rect x="80" y="140" width="20" height="20" transform="rotate(90 80 140)" fill="white"/>
            <rect x="100" y="140" width="20" height="20" transform="rotate(90 100 140)" fill="white"/>
            <rect x="120" y="140" width="20" height="20" transform="rotate(90 120 140)" fill="#D82800"/>
            <rect x="60" y="140" width="20" height="20" transform="rotate(90 60 140)" fill="white"/>
            <rect x="80" y="20" width="20" height="20" transform="rotate(90 80 20)" fill="white"/>
            <rect x="100" y="20" width="20" height="20" transform="rotate(90 100 20)" fill="#D82800"/>
            <rect x="80" y="180" width="20" height="20" transform="rotate(90 80 180)" fill="white"/>
            <rect x="100" y="180" width="20" height="20" transform="rotate(90 100 180)" fill="white"/>
            <rect x="120" y="180" width="20" height="20" transform="rotate(90 120 180)" fill="#D82800"/>
            <rect x="60" y="180" width="20" height="20" transform="rotate(90 60 180)" fill="white"/>
            <rect x="80" y="120" width="20" height="20" transform="rotate(90 80 120)" fill="white"/>
            <rect x="100" y="120" width="20" height="20" transform="rotate(90 100 120)" fill="white"/>
            <rect x="120" y="120" width="20" height="20" transform="rotate(90 120 120)" fill="#D82800"/>
            <rect x="60" y="120" width="20" height="20" transform="rotate(90 60 120)" fill="white"/>
            <rect x="80" y="60" width="20" height="20" transform="rotate(90 80 60)" fill="white"/>
            <rect x="100" y="60" width="20" height="20" transform="rotate(90 100 60)" fill="white"/>
            <rect x="120" y="60" width="20" height="20" transform="rotate(90 120 60)" fill="#D82800"/>
            <rect x="60" y="60" width="20" height="20" transform="rotate(90 60 60)" fill="white"/>
            <rect x="80" y="220" width="20" height="20" transform="rotate(90 80 220)" fill="white"/>
            <rect x="100" y="220" width="20" height="20" transform="rotate(90 100 220)" fill="white"/>
            <rect x="120" y="220" width="20" height="20" transform="rotate(90 120 220)" fill="#D82800"/>
            <rect x="60" y="220" width="20" height="20" transform="rotate(90 60 220)" fill="white"/>
            <rect x="80" y="80" width="20" height="20" transform="rotate(90 80 80)" fill="white"/>
            <rect x="100" y="80" width="20" height="20" transform="rotate(90 100 80)" fill="white"/>
            <rect x="120" y="80" width="20" height="20" transform="rotate(90 120 80)" fill="#D82800"/>
            <rect x="60" y="80" width="20" height="20" transform="rotate(90 60 80)" fill="white"/>
            <rect x="80" y="240" width="20" height="20" transform="rotate(90 80 240)" fill="white"/>
            <rect x="100" y="240" width="20" height="20" transform="rotate(90 100 240)" fill="#D82800"/>
        </svg>
    </div>


</div>