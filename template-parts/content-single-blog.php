<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theme-by-socreativ
 */

?>
<div class="title-page single-blog">
	<?php
	if ( have_rows('videos') ):
			while ( have_rows('videos') ) : the_row();
				if (get_row_index() == '1') {
					 $file = get_sub_field('File');
					 $firstfile = $file['source']['url'];
				 }
			 endwhile;
	endif; ?>
	<?php
	the_post_thumbnail('post-thumbnail', ['class' => 'object-fit-cover h-100 w-100']);
	?>
<div class="entry-title">
	<?php the_title( '<h1>', '</h1>' ); ?>
</div>

<div class="archive-blog-back">
	<a class="btn-full" href="<?=  get_post_type_archive_link(get_post_type()); ?>">← Retour à l'archive</a>
</div>

</div>
<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container single-container">
		<div class="position-relative single-parallax">
			<?php
			the_post_thumbnail('post-thumbnail', ['class' => 'object-fit-cover h-100 w-100 mb-5']);
			?>
		</div>
		<div class="is-style-container-small">
			<?php
			the_content();
			?>

			<?php $nextPost = get_next_post();
				  if($nextPost == ''){
					$first = new wp_query(array('post_type' => 'post', 'order' => 'ASC', 'posts_per_page' => 1));
					$nextPost = $first->posts[0];
				  }


			?>

			<a class="bold" href="<?= get_permalink($nextPost->ID) ?>">Article suivant : <?= $nextPost->post_title; ?></a>

		</div>


	</div>


</section><!-- #post-<?php the_ID(); ?> -->
