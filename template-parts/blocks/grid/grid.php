<?php if(have_rows('content')): ?>
<section id="<?= $block['anchor'] ?>" class="grid-block <?= $block['className'] ?>" block-slug="<?= $block['id'] ?>">
    <div class="container">
        <div class="grid">
            <?php while (have_rows('content')): the_row(); ?>







                <?php 
                $link = get_sub_field('text');
                if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                    <a class="target-card hover-glitch-container" id="target-card-<?= get_row_index(); ?>" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">

                    <div class="icon">
                        <?php if(wp_is_mobile()){?>
                            <?php if (get_sub_field('icon')){ ?>
                                <?= get_sub_field('icon'); ?>                                
                            <?php } ?>
                            <?php if (get_sub_field('titre')){ ?>
                                <span class="grid-title">
                                    <?= get_sub_field('titre'); ?>
                                </span>
                            <?php } ?>
                        <?php }else{ ?>
                            <?php if (get_sub_field('icon')){ ?>
                                <div class="glitch-container hover-glitch">
                                    <?= get_sub_field('icon'); ?>
                                    <?= get_sub_field('icon'); ?>
                                    <?= get_sub_field('icon'); ?>
                                </div>
                            <?php } ?>
                            <?php if (get_sub_field('titre')){ ?>
                                <span class="grid-title">
                                    <?= get_sub_field('titre'); ?>
                                </span>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <p class="label"><?php echo html_entity_decode($link_title); ?></p>
                </a>
                <?php endif; ?>
                






            <?php endwhile; ?>
        </div>
    </div>


    <div class="le-reglement lr1"></div>
    <div class="le-reglement lr2"></div>
    <div class="le-reglement lr3"></div>

</section>
<?php endif ;?>

