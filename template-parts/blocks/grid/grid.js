docReady(() => {
    const leReglement = document.querySelectorAll('.le-reglement');
    const block = document.querySelector('.grid-block');
    leReglement.forEach((e, i) => {
        const toReach = -100 * (i+1);
        gsap.to(e, 
            {ease: Power2.easeOut, y: toReach, scrollTrigger: 
                {
                  trigger: block,
                  start: 'top bottom',
                  end: 'bottom top',
                  scrub: true,
                }
            }
        )
    });
});