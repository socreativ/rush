"use strict";
docReady(() => {
    const svg = document.querySelectorAll('.svg-text svg');
    svg.forEach(e => {
       initCorrectSize(e)
       const text = e.children[0];
       gsap.to(text, 
          {ease: Power2.easeOut, strokeDashoffset: 250, scrollTrigger: 
            {
              trigger: e,
              start: 'top bottom-=100',
              end: 'bottom+=300 top+=300',
              scrub: true,
            }
          }
        );
    });

    function initCorrectSize(element){
      element.setAttribute('height', element.clientHeight);
      element.setAttribute('width', element.clientWidth);
      element.children[0].setAttribute('textLength', element.clientWidth);
    }
});