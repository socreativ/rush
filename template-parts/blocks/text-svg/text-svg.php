<?php

    if (wp_is_mobile()) {
        $margin_top = get_field('margin')['top']['value_mobile'];
        $margin_bottom = get_field('margin')['bottom']['value_mobile'];
        $heigth = get_field('dimension')['height_mobile'];
        $width = get_field('dimension')['width_mobile'];
    }else{
        $margin_top = get_field('margin')['top']['value'];
        $margin_bottom = get_field('margin')['bottom']['value'];
        $heigth = get_field('dimension')['height'];
        $width = get_field('dimension')['width'];
    }
    
    $margin_top_unity = get_field('margin')['top']['unite'];
    $margin_bottom_unity = get_field('margin')['bottom']['unite'];

?>

<section id="<?= $block['anchor'] ?>" class="svg-text <?= $block['className'] ?>" block-slug="<?= $block['id'] ?>"
 style="margin-top: <?= $margin_top . $margin_top_unity ?>;margin-bottom: <?= $margin_bottom . $margin_bottom_unity ?>;">
    <svg height="<?= $heigth ?>vh" width="<?= $width ?>vw">
        <text x="0" y="50%" textLength="<?= $width; ?>vw" style="font-size: <?= $heigth ?>vh" ><?= get_field('text'); ?></text>
    </svg>
</section>