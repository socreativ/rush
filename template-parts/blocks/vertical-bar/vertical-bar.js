"use strict";
docReady(() => {
    const verticalBars = document.querySelectorAll('.vertical-bar');
    verticalBars.forEach(e => {
        const animPart = e.querySelector('.animed');
        gsap.to(animPart, { ease: Power2.easeOut, scaleY: 1, 
            scrollTrigger: {
              trigger: e,
              start: 'top 60%',
              end: 'bottom 40%',
              scrub: true,
            } 
          })
    });
  });