

docReady(() => {

    const SPEED = 200;
    const coins = document.querySelectorAll('.coin');
    const section = document.querySelector('.coin-flip');
    const intervals = [];

    initObserver();
    initScrollTrigger();

    function initScrollTrigger(){
        coins.forEach((c, i) => {
            const posToReach = i === 1 ? '90vh' : Math.floor(Math.random() * 50) + 30 + 'vh';
            gsap.to(c.parentNode, {y: posToReach, ease: Power2.easeOut, scrollTrigger: {
                trigger: section,
                start: 'top+=450 center',
                end: 'bottom top',
                scrub: true,
            }})
        });
    }

    function initObserver(){
        const observer = new IntersectionObserver((entries, obersver) => {
            entries.forEach(entry => {
                if(entry.isIntersecting){
                    startAnimation();
                }
                else{
                    stopAnimation();
                }
            });
        }, {threshold: 0.1});
        observer.observe(section);
    }

    function startAnimation(){
        coins.forEach((c, i) => {
            const frames = c.querySelectorAll('.sprite');
            setTimeout(() => intervals.push(coinFlip(frames)), 100*i);
        });
    }

    function stopAnimation(){
        intervals.forEach(i => {
            clearInterval(i);
        });
        intervals.splice(0, intervals.length);
    }

    function coinFlip(sprites){
        let cFrame = 0;
        return setInterval(() => {
            sprites[cFrame].classList.remove('current');
            cFrame = nextFrame();
            sprites[cFrame].classList.add('current');
        }, SPEED);

        function nextFrame(){
            if(cFrame < sprites.length - 1){
                return cFrame+1;
            }
            if(cFrame === sprites.length -1){
                return 0;
            }
        }
    }

  


});