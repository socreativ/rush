<?php
    $margin_top_value = get_field('margin')['top']['value'];
    $margin_top_unity = get_field('margin')['top']['unite'];
    $margin_bottom_value = get_field('margin')['bottom']['value'];
    $margin_bottom_unity = get_field('margin')['bottom']['unite'];
?>

<section id="<?= $block['anchor'] ?>" class="coin-flip <?= $block['className'] ?>" block-slug="<?= $block['id'] ?>" duck="true"
style="margin-top: <?= $margin_top_value . $margin_top_unity ?>;margin-bottom: <?= $margin_bottom_value . $margin_bottom_unity ?>;">

    <div class="coin-1">
        <?php include(get_template_directory() . '/template-parts/customs/coin.php'); ?>
    </div>

    <div class="coin-2">
        <?php include(get_template_directory() . '/template-parts/customs/coin.php'); ?>
    </div>

    <div class="coin-3">
        <?php include(get_template_directory() . '/template-parts/customs/coin.php'); ?>
    </div>
    

    <div class="black-hole">
        <svg width="176" height="17" viewBox="0 0 176 17" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M14.9858 16.1293C15.6107 16.5792 16.3611 16.8213 17.1311 16.8213H158.869C159.639 16.8213 160.389 16.5792 161.014 16.1293L173.762 6.9506C176.655 4.86772 175.182 0.299549 171.617 0.299549H4.38283C0.818128 0.299549 -0.655321 4.86772 2.23756 6.95059L14.9858 16.1293Z" fill="black"/>
        </svg>
    </div>

</section>