<section id="<?= $block['anchor'] ?>" class="jumbotron <?= $block['className'] ?>" block-slug="<?= $block['id'] ?>">



    <h2 class="squada"><?= get_field('titre'); ?></h2>
    <h1 class="squada"><?= get_field('texte_neon'); ?></h1>

    <?php if (!my_wp_is_mobile()) { ?>
        <div class="icon">

            <div class="top-right corner">
                <div class="glitch-container">
                    <svg width="22" height="11" viewBox="0 0 22 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.75 1L20.75 1L20.75 11" stroke="white" stroke-width="1.25"/>
                    </svg>
                    <svg width="22" height="11" viewBox="0 0 22 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.75 1L20.75 1L20.75 11" stroke="white" stroke-width="1.25"/>
                    </svg>
                    <svg width="22" height="11" viewBox="0 0 22 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.75 1L20.75 1L20.75 11" stroke="white" stroke-width="1.25"/>
                    </svg>
                </div>
            </div>
                <div class="main-icon">
                    <div class="glitch-container">
                    <?= get_field('icon'); ?>
                    <?= get_field('icon'); ?>
                    <?= get_field('icon'); ?>
                    </div>
                </div>

            <div class="bottom-left corner">
                <div class="glitch-container">
                    <svg width="22" height="11" viewBox="0 0 22 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M21.25 10L1.25 10L1.25 -3.02996e-06" stroke="white" stroke-width="1.25"/>
                    </svg>
                    <svg width="22" height="11" viewBox="0 0 22 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M21.25 10L1.25 10L1.25 -3.02996e-06" stroke="white" stroke-width="1.25"/>
                    </svg>
                    <svg width="22" height="11" viewBox="0 0 22 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M21.25 10L1.25 10L1.25 -3.02996e-06" stroke="white" stroke-width="1.25"/>
                    </svg>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="cta">
        <?php if(have_rows('boutons')): ?>
            <?php while(have_rows('boutons')): the_row(); ?>
                <div class="<?= get_sub_field('css'); ?>">
                    <a href="<?= get_sub_field('btn')['url'] ?>">
                    <img src="<?= get_sub_field('icon')['url'] ?>" alt="<?= get_sub_field('icon')['alt'] ?>">
                    <?= get_sub_field('btn')['title']; ?>
                    </a>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>

</section>