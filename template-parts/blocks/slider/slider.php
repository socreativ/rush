<?php

$image_single = get_field('image_single');
$video = get_field('video');
$alpha = get_field('alpha') / 100;

$title = get_field('title')['text'];
$title_css = get_field('title')['css'];
$title_el = get_field('title')['balise'];
$title_attr = get_field('title')['attr'];

$subtitle = get_field('subtitle')['text'];
$subtitle_css = get_field('subtitle')['css'];
$subtitle_el = get_field('subtitle')['balise'];
$subtitle_attr = get_field('subtitle')['attr'];

# short text
$short = get_field('short');

?>

<!-- SLIDER BEGIN -->
<section <?php if(isset($block['anchor'])) echo 'id="'.$block['anchor'].'"'; ?> class="slider <?php if(isset($block['className'])) echo $block['className']; ?>">

    <!-- <img src="<?php echo $image_single['url'] ?>" alt="<?php echo $image_single['alt'] ?>"/> -->
    <?php if(!my_wp_is_mobile()){?>
      <video autoplay muted loop src="<?= $video ?>"></video>
      <div class="greyscale" style="opacity: <?= $alpha ?>"></div>

      <div class="full-video">
        <?php include(get_template_directory() . '/template-parts/customs/fullScreen-btn.php'); ?>
      </div>

      <div class="close-full-video hidden gaming-frame hover-glitch-container">
        <?php if(wp_is_mobile()){?>
          <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"><title>Close fullscreen</title>
            <g class="nc-icon-wrapper" fill="#ffffff">
              <path d="M38 12.83L35.17 10 24 21.17 12.83 10 10 12.83 21.17 24 10 35.17 12.83 38 24 26.83 35.17 38 38 35.17 26.83 24z"/>
            </g>
          </svg>
        <?php }else{ ?>
         <div class="glitch-container hover-glitch">
          <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"><title>Close fullscreen</title>
            <g class="nc-icon-wrapper" fill="#ffffff">
              <path d="M38 12.83L35.17 10 24 21.17 12.83 10 10 12.83 21.17 24 10 35.17 12.83 38 24 26.83 35.17 38 38 35.17 26.83 24z"/>
            </g>
          </svg>
          <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"><title>Close fullscreen</title>
            <g class="nc-icon-wrapper" fill="#ffffff">
              <path d="M38 12.83L35.17 10 24 21.17 12.83 10 10 12.83 21.17 24 10 35.17 12.83 38 24 26.83 35.17 38 38 35.17 26.83 24z"/>
            </g>
          </svg>
          <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"><title>Close fullscreen</title>
            <g class="nc-icon-wrapper" fill="#ffffff">
              <path d="M38 12.83L35.17 10 24 21.17 12.83 10 10 12.83 21.17 24 10 35.17 12.83 38 24 26.83 35.17 38 38 35.17 26.83 24z"/>
            </g>
          </svg>
        </div>
        <?php } ?>
      </div>
    <?php } ?>
          
    <div class="slider-content">
         <div class="slider-text appear">
            <?php if($title_el): ?>
              <<?php echo $title_el; ?> class="<?php echo $title_css; ?>" <?php echo $title_attr; ?>><?php echo $title; ?></<?php echo $title_el; ?>>
            <?php endif; ?>
 
            <?php if($subtitle_el): ?>
              <<?php echo $subtitle_el; ?> class="<?php echo $subtitle_css; ?>" <?php echo $subtitle_attr; ?>><?php echo $subtitle; ?></<?php echo $subtitle_el; ?>>
            <?php endif; ?>

            <div class="short">
                <?= $short ?>
            </div>
  

        <?php if(have_rows('btn')):
              while(have_rows('btn')): the_row();
                  $link = get_sub_field('link');
                  $css = get_sub_field('css_class'); ?>
                  <a href="<?php echo $link['url']; ?>" class="<?php echo $css; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
              <?php endwhile;
          endif; ?>
      </div>

    </div>


</section>
<!-- END SLIDER -->
