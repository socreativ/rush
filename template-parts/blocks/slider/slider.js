"use strict";

function initHomeVideo(){

    const openBtn = document.querySelector('.full-video');
    const closeBtn = document.querySelector('.close-full-video');
    const hideEl = document.querySelectorAll('header, nav, .slider-content, .full-video, .greyscale');
    
    initListener();

    function openVideo(){
        window.scrollTo({top: 0, behavior: 'smooth'});
        hideEl.forEach(e => e.classList.add('hidden'));
        closeBtn.classList.remove('hidden');
        document.body.style.overflow = 'hidden';
    }

    function closeVideo(){
        hideEl.forEach(e => e.classList.remove('hidden'));
        closeBtn.classList.add('hidden');
        document.body.style.overflow = 'auto';
    }

    function initListener(){
        openBtn.addEventListener('click', openVideo);
        closeBtn.addEventListener('click', closeVideo);
    }

    function removeListener(){
        openBtn.removeEventListener('click', openVideo);
        closeBtn.removeEventListener('click', closeVideo);
    }

}


docReady(() => {
    initHomeVideo();
});