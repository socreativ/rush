docReady(() => {

    const menuItems = document.querySelectorAll('.menu-selector .menu-item');
    const follower = document.querySelector('.menu-follower');
    let currentItem = document.querySelector('.menu-selector .current-menu-item')
    initFollower();
    initListener();

    function initListener() {
        menuItems.forEach(item => {
            item.addEventListener('mouseenter', trackItem);
        });
    }

    function trackItem(e) {
        if (currentItem !== e.target) {
            currentItem.classList.remove('current-menu-item');
            e.target.classList.add('current-menu-item');
            currentItem = e.target;
            setPosAndWidth(follower, currentItem);
        }
    }

    function initFollower() {
        setPosAndWidth(follower, currentItem);
        setTimeout(() => follower.animate([{ opacity: 1 }], { duration: 350, fill: 'forwards' }), 350);
    }

    function setPosAndWidth(element, target) {
        element.setAttribute('style', `width: ${getWidth(target)}px;left: ${getPos(target)}px;`);
    }

    function getPos(element) {
        return element.offsetLeft;
    }

    function getWidth(element) {
        return element.clientWidth;
    }

});

