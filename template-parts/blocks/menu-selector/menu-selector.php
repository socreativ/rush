<section id="<?= $block['anchor'] ?>" class="menu-selector <?= $block['className'] ?>" block-slug="<?= $block['id'] ?>">

    <?php wp_nav_menu(array('theme_location' => get_field('menu'))); ?>

    <div class="menu-follower"></div>

</section>