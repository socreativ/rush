<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theme-by-socreativ
 */


if($args[0] % 2 == 0){
    $scrollspeed = "data-scroll-speed='1'";
    $parralax = 0;       
    $margin = 3;
}
else{
    $scrollspeed = "data-scroll-speed='2'";
    $parralax = 1;
    $margin = 0;
}
?>
    <article id="post-<?php the_ID(); ?>" <?php post_class('opacity-appear w-100 even-div col-12 col-md-12 col-lg-6 d-flex align-items-end single-link-container h-100 my-'. $margin); ?> parallax="<?= $parralax ?>">
        <div class="cover w-100 h-100">
             <a href="<?php the_permalink(); ?>" class="link-to-post d-flex anim-300 position-relative overflow-hidden p-3">

                <div class="entry-header w-100 d-flex align-items-center">
                 <div class="w-100">
                    <?php
                    $TodayTime = strtotime(date('m/d/y'));
                    $EventTime = strtotime(get_field('event_date'));
                    $EventEndTime = strtotime(get_field('event_end_date'));
                    $DayEventDate = strtotime(get_field('event_date'));
                    $DayEventEndDate = strtotime(get_field('event_end_date'));?>

                    <div class="">
                    <?php if(get_field('event_date')): ?>
                        <?php if ($DayEventDate == $DayEventEndDate): ?>
                                <?=  date('d', $DayEventEndDate); ?> <?=  date('M', $DayEventEndDate); ?> <?=  date('Y', $DayEventEndDate); ?>
                        <?php else: ?>
                                <?=  date('d', $DayEventDate); ?> - <?=  date('d', $DayEventEndDate); ?> <?=  date('M', $DayEventEndDate); ?> <?=  date('Y', $DayEventEndDate); ?>
                        <?php endif; ?>
                    <?php else : ?>
                        <?=  get_the_date(); ?>
                    <?php endif; ?>
                    </div>
                    <div class="d-flex">
                        <span class="w-100 title-article">
                            <?php $title = the_title('','',FALSE); echo substr($title, 0);?>
                        </span>
                    </div>
                </div>
                <div class="ml-3 anim-300 p-2 mb-0">
                    <img src="<?=  get_stylesheet_directory_uri() . "/assets/img/arrow.svg"; ?>" alt="">
                </div>
            </div>
            <img class="archive-img-cover anim-300" src="<?=  get_the_post_thumbnail_url(); ?>" alt="">
        </a>
        </div>
    </article><!-- #post-<?php the_ID(); ?> -->

