<?php 

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

         // Espacement
         acf_register_block_type(array(
            'name'              => 'spacing',
            'title'             => __('Espacement'),
            'description'       => __('margin block'),
            'category'          => 'custom-blocks',
            'icon'              => 'editor-break',
            'keywords'          => array( 'margin', 'space', 'vh'),
            'render_template'   => '/template-parts/blocks/spacing/spacing.php',
            'supports'          => array( 'anchor' => true),
        ));
        acf_register_block_type(array(
            'name'              => 'vertical-bar',
            'title'             => __('Vertical Bar'),
            'description'       => __('Vertical Bar'),
            'render_template'   => 'template-parts/blocks/vertical-bar/vertical-bar.php',
            'category'          => 'custom-blocks',
            'icon'              => 'tide',
            'keywords'          => array( 'Vertical bar', 'margin', 'vertical'),
            'supports'          => array( 'anchor' => true),
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/vertical-bar/vertical-bar.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/vertical-bar/vertical-bar.css',
        ));
 
        acf_register_block_type(array(
            'name'              => 'slider',
            'title'             => __('Slider'),
            'description'       => __('Slider'),
            'render_template'   => 'template-parts/blocks/slider/slider.php',
            'category'          => 'custom-blocks',
            'icon'              => 'format-gallery',
            'keywords'          => array( 'slider', 'video', 'picture'),
            'supports'          => array( 'anchor' => true),
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/slider/slider.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/slider/slider.css',
        ));

        acf_register_block_type(array(
            'name'              => 'block-agrandissable',
            'title'             => __('block-agrandissable'),
            'description'       => __('block agrandissable'),
            'category'          => 'custom-blocks',
            'icon'              => 'admin-comments',
            'render_template'   => 'template-parts/blocks/block-agrandissable/content-block-agrandissable.php',
            'supports'          => array( 'anchor' => true),
        ));

        acf_register_block_type(array(
            'name'              => 'text-draw',
            'title'             => __('Texte SVG'),
            'description'       => __('Texte svg animé'),
            'category'          => 'custom-blocks',
            'icon'              => 'editor-textcolor',
            'render_template'   => 'template-parts/blocks/text-svg/text-svg.php',
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/text-svg/text-svg.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/text-svg/text-svg.css',
            'supports'          => array( 'anchor' => true),
        ));

       acf_register_block_type(array(
            'name'              => 'card-grid',
            'title'             => __('Grille'),
            'description'       => __('Grille de carte'),
            'category'          => 'custom-blocks',
            'icon'              => 'grid-view',
            'render_template'   => 'template-parts/blocks/grid/grid.php',
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/grid/grid.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/grid/grid.css',
            'supports'          => array( 'anchor' => true),
        ));
        
        acf_register_block_type(array(
            'name'              => 'testimonial',
            'title'             => __('Témoignage'),
            'description'       => __('Témoigne, avis'),
            'category'          => 'custom-blocks',
            'icon'              => 'grid-view',
            'keywords'          => array( 'testimony', 'commentary', 'review', 'avis', 'google' ),
            'render_template'   => 'template-parts/blocks/testimonial/testimonial.php',
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/testimonial/testimonial.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/testimonial/testimonial.css',
            'supports'          => array( 'anchor' => true),
        ));

        acf_register_block_type(array(
            'name'              => 'coin-flip',
            'title'             => __('Tirelire à pièce'),
            'description'       => __('La tite tirelire cochon avec des pièces'),
            'category'          => 'custom-blocks',
            'icon'              => 'money-alt',
            'render_template'   => 'template-parts/blocks/coin-flip/coin-flip.php',
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/coin-flip/coin-flip.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/coin-flip/coin-flip.css',
            'supports'          => array( 'anchor' => true),
        ));
        
        acf_register_block_type(array(
            'name'              => 'menu-selector',
            'title'             => __('Menu selector'),
            'description'       => __('Menu'),
            'category'          => 'custom-blocks',
            'icon'              => 'menu',
            'render_template'   => 'template-parts/blocks/menu-selector/menu-selector.php',
            'enqueue_script' => get_template_directory_uri() . '/template-parts/blocks/menu-selector/menu-selector.js',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/menu-selector/menu-selector.css',
            'supports'          => array( 'anchor' => true),
        ));

        acf_register_block_type(array(
            'name'              => 'jumbotron',
            'title'             => __('Jumbotron'),
            'description'       => __('Menu'),
            'category'          => 'custom-blocks',
            'icon'              => 'admin-home',
            'render_template'   => 'template-parts/blocks/jumbotron/jumbotron.php',
            'enqueue_style' => get_template_directory_uri() . '/template-parts/blocks/jumbotron/jumbotron.css',
            'supports'          => array( 'anchor' => true),
        ));

        add_action('acf/init', 'register_acf_block_types');

    }


?>