("use strict");
(function ($, root, undefined) {
  $(function () {
    $(document).ready(function () {
      console.log('LOADED');
      var isMobile = window.innerWidth < 768 ? true : false;
      /**
      *   Blog
      */

      $('.single-link-container').each(function (){
        let bkg = $(this).find('.archive-img-cover').attr('src');
        let target = $('.attachment-post-thumbnail');

        $(this).on('mouseover', function (){

          if(target.attr('src') != bkg){
            TweenMax.to(".attachment-post-thumbnail", 0.3, {
              autoAlpha: 0,
              scale: 1.05
            });

            target.attr('src', bkg);

            setTimeout(() => {
              TweenMax.to(".attachment-post-thumbnail", 0.3, {
                autoAlpha: 1,
                scale: 1
              });
            }, 300);
          }
        });
      });

      if(!isMobile){
        $('.single-link-container[parallax="1"]').each(function (){
          gsap.to(this, {y: -150, scrollTrigger: {
            trigger: document.querySelector('.container-index'),
            start: "top top",
            end: "bottom top",
            scrub: true,
          }});
        });

        $('.single-parallax').each(function (){
          gsap.to(this, {y: -150, scrollTrigger: {
            trigger: document.querySelector('.single-container'),
            start: "top top",
            end: "bottom top",
            scrub: true
          }});
        });
      }

      /**
       *  CPT Archive & Single
       */

      $('.filtre-toggle').each(function (){
        $(this).click(function (){
          $('.filtre').toggleClass('open');
        })
      })

      $('.archive-single-product').each(function (){
        let bkg = $(this).find('.wp-post-image').attr('src');
        let target = $('.attachment-post-thumbnail.archive-bkg');

        $(this).on('mouseover', function (){

          if(target.attr('src') != bkg){
            TweenMax.to(".attachment-post-thumbnail.archive-bkg", 0.3, {
              autoAlpha: 0,
              scale: 1.05
            });

            target.attr('src', bkg);

            setTimeout(() => {
              TweenMax.to(".attachment-post-thumbnail.archive-bkg", 0.3, {
                autoAlpha: 1,
                scale: 1
              });
            }, 300);
          }
        })
      })

      $('.desc-tab-tgl').click(function (){
        $('.desc-tab').removeClass('d-none').addClass('d-block');
        $('.additional-tab').removeClass('d-block').addClass('d-none');
        $(this).addClass('active');
        $('.additional-tab-tgl').removeClass('active');
      });
      $('.additional-tab-tgl').click(function (){
        $('.additional-tab').removeClass('d-none').addClass('d-block');
        $('.desc-tab').removeClass('d-block').addClass('d-none');
        $(this).addClass('active');
        $('.desc-tab-tgl').removeClass('active');
      })

      $('.product-gallery-sticky').each(function (){
        $(this).slick({
          autoplay: false,
          dots: true,
          fade: false,
          infinite: false
        })
      })
    });
  });
})(jQuery, this);
