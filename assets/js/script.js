"use strict";

function docReady(fn) {
    if (document.readyState === "complete" || document.readyState === "interactive") {
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

function getRdm(max, min = 0) {
    return Math.floor(Math.random() * max) + min;
}

function initLoader() {

    const loader = document.querySelector('#loader');
    const now = new Date();
    if (localStorage.getItem('rush_loader') !== null && now < new Date(parseInt(localStorage.getItem('rush_loader')))) {
        loader.remove();
    }
    else {
        Loader();
    }

    function Loader() {
        loader.classList.remove('hide');
        const frames = document.querySelectorAll('.frame:not(.frame-1)');
        const speedTrap = addSpeedEffect(loader, 5);
        gsap.to(speedTrap, { y: -window.innerHeight * 1.5, duration: 0.2, delay: 1, repeat: frames.length })
        frames.forEach((f, i) => {
            setTimeout(() => {
                if (i === 0) setTimeout(() => document.querySelector('#loader .text').classList.add('white'), 100);
                f.animate([{ transform: 'translateY(-100vh)' }], { duration: 200, ease: 'linear', fill: 'forwards' });
            }, 1000 + (i * 200));
        });
        setTimeout(() => {
            loader.classList.add('fadeout');
            let expiration = new Date();
            expiration.setDate(now.getDate() + 1);
            localStorage.setItem('rush_loader', expiration.getTime());
            setTimeout(() => loader.remove(), 350);
        }, 900 + ((frames.length - 1) * 400));
    }
}

function addSpeedEffect(parent, nbr) {
    const elements = [];
    for (let i = 0; i < nbr; i++) {
        let rdmOffset = Math.floor(Math.random() * 5);
        rdmOffset *= Math.round(Math.random()) ? 1 : -1;
        const leftPos = Math.floor(100 / (nbr + 1) * (i + 1)) + rdmOffset;
        const topPos = getRdm(130, 100);
        const e = createSpeedElement(`${parent.id}-${i}`, leftPos, topPos);
        elements.push(e)
        parent.append(e);
    }
    return elements;
}

function createSpeedElement(id, left, top) {
    const element = document.createElement('div');
    const i = id.match(/\d+/)[0];
    element.id = 'speed-trap-' + id;
    element.classList.add('speed-trap');
    element.style.left = left + 'vw';
    element.style.top = top + 'vh';
    element.style.transform = 'translateY(0px)';
    element.setAttribute('speed', getRdm(3, 1));
    element.innerHTML = // sorry not sorry
        `<svg width="20" height="355" viewBox="0 0 20 355" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M10 0C15.5228 0 20 4.47715 20 10L20 345C20 350.523 15.5229 355 10 355C4.47717 355 1.52588e-05 350.523 1.43051e-05 345L0 10C0 4.47715 4.47715 0 10 0Z" fill="url(#paint0_linear_146_1340)"/>
            <defs>
                <linearGradient id="paint0_linear_146_1340" x1="20.0044" y1="0.000125063" x2="10.0048" y2="355" gradientUnits="userSpaceOnUse">
                    <stop stop-color="#808080" stop-opacity="0.1"/>
                    <stop offset="0.442708" stop-color="#808080"/>
                    <stop offset="1" stop-color="#808080" stop-opacity="0.1"/>
                </linearGradient>
            </defs>
        </svg>`;
    return element;
}

function listenForSticky() {
    const footer = document.querySelector('footer');
    window.addEventListener('scroll', () => {
        if (window.scrollY < 150) {
            document.body.classList.remove('sticky');
        }
        else {
            document.body.classList.add('sticky');
        }
        if (window.scrollY > document.body.scrollHeight - footer.clientHeight - (window.innerHeight / 2)) {
            document.body.classList.add('footerReached');
        }
        else {
            document.body.classList.remove('footerReached');
        }
    });
}

function listenForNavMenu() {
    const navTriggerOpen = document.querySelector('.site-navigation');
    const navTriggerClose = document.querySelector('.close-site-navigation');
    const submenuItems = document.querySelectorAll('.sub-menu li');

    navTriggerOpen.addEventListener('click', openMenu);
    navTriggerClose.addEventListener('mousedown', closeMenu);

    submenuItems.forEach(item => {
        item.addEventListener('mouseover', addActive);
        item.addEventListener('mouseleave', removeActive);
    });

    function addActive(e) {
        e.target.parentNode.parentNode.classList.add('child-active');
    }

    function removeActive(e) {
        e.target.parentNode.parentNode.classList.remove('child-active');
    }

    function closeMenu() {
        document.body.classList.remove('site-navigation-child-open');
        setTimeout(() => document.body.classList.remove('site-navigation-open'), 300);
        setTimeout(() => navTriggerOpen.addEventListener('click', openMenu), 700);
    }

    function openMenu() {
        document.body.classList.add('site-navigation-open');
        setTimeout(() => document.body.classList.add('site-navigation-child-open'), 700);
        navTriggerOpen.removeEventListener('click', openMenu);
    }
}

function listenForFaq() {
    const faqQuestion = document.querySelectorAll('.schema-faq-question');
    faqQuestion.forEach(f => {
        f.addEventListener('click', () => {

            const answer = f.parentNode.children[1];
            f.classList.toggle('question-open');
            if (f.classList.contains('question-open')) {
                answer.style.display = 'block';
            }
            else {
                answer.style.display = 'none';
            }
        });
    });
}

function initSpeedDrop() {
    const isDisabled = document.body.classList.contains('no-speedtrap');
    if (!isDisabled) {
        const repeat = Math.floor(document.scrollingElement.scrollHeight / 1000);
        const speedElements = addSpeedEffect(document.body, 6);
        speedElements.forEach((e, i) => {
            const speed = e.getAttribute('speed');
            const toScroll = (window.innerHeight * 2) + (e.clientHeight * speed);
            gsap.to(e, {
                y: -toScroll, delay: 0.9 * i, repeat: repeat, scrollTrigger: {
                    trigger: window.body,
                    start: 'top top',
                    end: 'bottom bottom',
                    scrub: true,
                }
            });
        });
    }
}

function contactMap() {
    const mapElement = document.querySelector('.map');
    if (mapElement) {
        loadScript(
            'https://maps.googleapis.com/maps/api/js?key=AIzaSyDWOehSHBe4Ykhvz03_7WUC2vDenIGeP6Y',
            'googleMapApi-js',
            () => {
                const styleUrl = mapElement.getAttribute('style-location');
                loadScript(styleUrl, 'mapStyle-js', () => {
                    const mapStyle = getMapStyle();
                    const map = initMap(mapElement, mapStyle);
                    const infoWindow = initInfoWindow();
                    const marker = initMarker(map, { lat: 44.816744, lng: -0.590230 });
                    initMarkerListeners();

                    function openInfo() {
                        infoWindow.open({ anchor: marker, map, shouldFocus: false });
                    }

                    function initMarkerListeners() {
                        marker.addListener('click', openInfo)
                        setTimeout(openInfo, 1000);
                    }

                    function initMarker(map, pos) {
                        return new google.maps.Marker({
                            position: pos,
                            map: map,
                            // icon: 
                        });
                    }

                    function initInfoWindow() {
                        return new google.maps.InfoWindow({
                            content: '<p style="color:black">les infos</p>',
                            maxWidth: 300,
                        });
                    }

                    function initMap(element, mapStyle) {
                        return new google.maps.Map(element, {
                            zoom: 18,
                            center: {
                                lat: 44.817273,
                                lng: -0.590230
                            },
                            streetViewControl: false,
                            mapTypeControl: false,
                            style: mapStyle
                        });
                    }
                });
            }
        );
    }

    function loadScript(url, id, callback) {
        const script = document.createElement('script');
        script.src = url;
        script.id = id;
        script.onload = () => {
            callback();
        }
        document.body.append(script);
    }
}

docReady(() => {
    initLoader();
    initSpeedDrop();
    listenForSticky();
    listenForNavMenu();
    listenForFaq();
    contactMap();
});

