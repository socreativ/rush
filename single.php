<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package theme-by-socreativ
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php
		while ( have_posts() ) :

			the_post();

			get_template_part( 'template-parts/content-single-blog', get_post_type() );



		endwhile; // End of the loop.
		?>

	</main>
	<div class="opacity-appear navigation-div">
			<div class="text-black">
				<?php the_posts_navigation(); ?>
			</div>
		</div>
	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
