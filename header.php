<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package theme_by_socreativ
 */

 if(!isset($args['css'])) $args = [];

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!-- <link rel="stylesheet" href="https://use.typekit.net/zbe5pju.css"> -->
    <?php echo get_field('head_scripts', 'options') ?>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<?php the_field( "google_font","option" ); ?>
	<?php wp_head(); ?>
    <style>
        :root{--font: <?= get_field('fontname', 'options') ?>}
    </style>
</head>

<body <?php body_class($args['css']); ?>>

<?php wp_body_open(); ?>
<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'theme_by_socreativ' ); ?></a>

<?php include(get_template_directory() . '/template-parts/customs/loader.php'); ?>

<header id="masthead" class="site-header">
	<div class="logo">
		<a href="<?= get_home_url(); ?>">
			<?= get_field('logo', 'options')['svg'] ?>
		</a>
	</div>

	<h2><?php the_field('desc','options'); ?></h2>

	<a href="" class="btn-stroke">RESERVER</a>
</header>


<nav class="site-navigation">

	
	<div class="site-navigation-after">
		<div class="nav-section">
			<div class="close-site-navigation gaming-frame">
				<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"><title>Close menu</title>
					<g class="nc-icon-wrapper" fill="#ffffff">
					<path d="M38 12.83L35.17 10 24 21.17 12.83 10 10 12.83 21.17 24 10 35.17 12.83 38 24 26.83 35.17 38 38 35.17 26.83 24z"/>
					</g>
				</svg>
			</div>
			<?php 
				$image = get_field('image_menu_open','option');
				if( !empty( $image ) ): ?>
					
				<?php endif; ?>
			<div class="main-nav-container">
				<?php wp_nav_menu(array('theme_location' => 'main-menu', 'walker' => new Socreativ_Walker_Nav_Menu)); ?>
			</div>
		</div>
		<div class="main-nav-image-container">
			<?php #acf_img(); ?>
			<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
		</div>
	</div>
	<div class="tab">
		<span>MENU</span>	
	</div>
</nav>