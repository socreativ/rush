<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package theme_by_socreativ
 */

?>


<footer id="colophon" class="site-footer container-fluid">
    <div class="container">
        <div class="site-description">
            <div class="logo">
                <?= get_field('logo', 'options')['svg']; ?>
            </div>

            <div class="tagline text-center text-md-right squada">
                <?= get_bloginfo('description'); ?>
            </div>
        </div>

        <hr>

        <div class="footer-widget">
            <?php dynamic_sidebar('footer'); ?>
        </div>

        <hr>

        <div class="footer-menu">
            <?php wp_nav_menu(array('theme_location' => 'footer-menu')); ?>
        </div>

        <hr>

        <div class="sub-footer">
            <div class="RS-menu">
            <?php wp_nav_menu(array('theme_location' => 'rs-menu')); ?>
            </div>

            <div class="subfooter-menu">
            <?php wp_nav_menu(array('theme_location' => 'subfooter-menu')); ?>
            </div>
        </div>
    </div>
</footer><!-- #colophon -->


<?php wp_footer(); ?>
<?php echo get_field('footer_scripts', 'options') ?>
</body>
</html>
