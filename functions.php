<?php
/**
 * theme_by_socreativ functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package theme_by_socreativ
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'theme_by_socreativ_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function theme_by_socreativ_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on theme_by_socreativ, use a find and replace
		 * to change 'theme_by_socreativ' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'theme_by_socreativ', get_template_directory() . '/languages' );

		// remove php warnings
		error_reporting(E_ERROR | E_PARSE);

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'main-menu' => esc_html__( 'Primary', 'theme_by_socreativ' ),
				'footer-menu' => esc_html__( 'Footer', 'theme_by_socreativ' ),
				'rs-menu' => esc_html__( 'Social Media', 'theme_by_socreativ' ),
				'subfooter-menu' => esc_html__( 'Sub Footer', 'theme_by_socreativ' ),
				'page-inner-menu' => esc_html__( 'Page inner', 'theme_by_socreativ' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'theme_by_socreativ_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'theme_by_socreativ_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function theme_by_socreativ_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'theme_by_socreativ_content_width', 640 );
}
add_action( 'after_setup_theme', 'theme_by_socreativ_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function theme_by_socreativ_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer', 'theme_by_socreativ' ),
			'id'            => 'footer',
			'description'   => esc_html__( 'Footer widget', 'theme_by_socreativ' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>'
		)
	);
}
add_action( 'widgets_init', 'theme_by_socreativ_widgets_init' );




// include custom jQuery
function shapeSpace_include_custom_jquery() {
	if ( is_front_page() && is_home() ) {
	  // Default homepage
	} elseif ( is_front_page() ) {
	  // static homepage
	} elseif ( is_home() || is_single() || is_archive() ) {
		wp_deregister_script('jquery');
		wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, true);
	} else {
		  //everyting else
	}

}
add_action('wp_enqueue_scripts', 'shapeSpace_include_custom_jquery');




function theme_by_socreativ_scripts() {
	wp_enqueue_style( 'theme_by_socreativ-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'theme_by_socreativ-style', 'rtl', 'replace' );

	wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.custom.min.css', true, '1.0', 'all');
	wp_enqueue_style( 'helpers', get_stylesheet_directory_uri() . '/assets/css/helpers.css', true, '1.0', 'all');
	wp_enqueue_style( 'gutenberg', get_stylesheet_directory_uri() . '/assets/css/gutenberg.css', true, '1.0', 'all');
	wp_enqueue_style( 'header', get_stylesheet_directory_uri() . '/assets/css/header.css', true, '1.0', 'all');
	wp_enqueue_style( 'footer', get_stylesheet_directory_uri() . '/assets/css/footer.css', true, '1.0', 'all');
	wp_enqueue_style( 'contact', get_stylesheet_directory_uri() . '/assets/css/contact.css', true, '1.0', 'all');
	wp_enqueue_style( 'glitch', get_stylesheet_directory_uri() . '/assets/css/glitch.css', true, '1.0', 'all');

	// CONDITIONAL CSS
	if ( is_front_page() && is_home() ) {
	  // Default homepage
	} elseif ( is_front_page() ) {
	  // static homepage
	} elseif ( is_home() || is_single() || is_archive() ) {
	  // blog page
		wp_enqueue_style( 'blog', get_stylesheet_directory_uri() . '/assets/css/blog.css', true, '1.0', 'all');
	} else {
	  //everyting else
	}


	wp_enqueue_script( 'gsap', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/gsap.min.js', [], '1.0.0', true);
	wp_enqueue_script( 'ScrollTrigger', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/ScrollTrigger.min.js', [], '1.0.0', true);

	wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/script.js', array(), '1.0.0', true );


	wp_enqueue_script( 'theme_by_socreativ-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	// CONDITIONAL JS
	if ( is_front_page() && is_home() ) {
		// Default homepage
	} elseif ( is_front_page() ) {
		// static homepage
	} elseif ( is_home() || is_archive() || is_category()) {
		// blog page
		wp_enqueue_script( 'blogJs', get_template_directory_uri() . '/assets/js/blog.js', array(), '1.0.0', true );
	} else {
		//everyting else
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'theme_by_socreativ_scripts' );




/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/* Allow SVG files */
function wpc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'wpc_mime_types');

/* -------------------------------------------
				GUTENBERG STYLE
--------------------------------------------- */

// Gutenberg Editor width
function gb_gutenberg_admin_styles() {
    echo '
        <style>
            /* Main column width */
            .wp-block {
                max-width: 1220px;
            }

            /* Width of "wide" blocks */
            .wp-block[data-align="wide"] {
                max-width: 1980px;
            }

            /* Width of "full-wide" blocks */
            .wp-block[data-align="full"] {
                max-width: none;
            }
        </style>
    ';
}
add_action('admin_head', 'gb_gutenberg_admin_styles');


// GUTENBERG
// GROUP
register_block_style(
	'core/group',
	array(
		'name'  => 'container',
		'label' => __( 'group container on grid', 'wp-group-container' ),
		)
	);
	register_block_style(
	'core/group',
		array(
			'name'  => 'container-small',
			'label' => __( 'group container small center', 'wp-group-small-container' ),
		)
	);
	register_block_style(
	'core/group',
		array(
			'name'  => 'container-half-right',
			'label' => __( 'group container half right', 'wp-group-half-right-container' ),
		)
	);
	register_block_style(
	'core/group',
		array(
			'name'  => 'container-half-left',
			'label' => __( 'group container half left', 'wp-group-half-left-container' ),
		)
	);
	register_block_style(
	'core/group',
		array(
			'name'  => 'container-appear-on-scroll',
			'label' => __( 'group container appear on scroll', 'wp-group-appear-container' ),
		)
	);
	// BANNIERE
	register_block_style(
	'core/column',
		array(
			'name'  => 'custom-cover-col',
			'label' => __( 'custom-Cover-col', 'wp-cover-elisa' ),
		)
	);
	// BUTTON
	register_block_style(
	'core/button',
		array(
			'name'  => 'btn-full',
			'label' => __( 'btn-full', 'btn-full' ),
		)
	);




/*********************************************************************
                        PLUGIN DEPENDENCIES
 ********************************************************************/

require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'socreativ_register_required_plugins' );


function socreativ_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// This is an example of how to include a plugin bundled with a theme.
		array(
			'name'               => 'Advanced Custom Fields Pro', // The plugin name.
			'slug'               => 'advanced-custom-fields-pro', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/inc/plugins/advanced-custom-fields-pro.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),

		array(
			'name'        => 'WordPress SEO by Yoast',
			'slug'        => 'wordpress-seo'
		),

		array(
			'name'        => 'Contact Form 7',
			'slug'        => 'contact-form-7'
		),

		array(
			'name'        => 'Flamingo',
			'slug'        => 'flamingo'
		),

		array(
			'name'        => 'iThemesecurity',
			'slug'        => 'better-wp-security',
			'force_activation'   => true
		),

		array(
			'name'        => 'ACF Content Analyse for Yoast SEO',
			'slug'        => 'acf-content-analysis-for-yoast-seo'
		),

		array(
			'name'        => 'Yoast Duplicate Post',
			'slug'        => 'duplicate-post',
		)

	);


	$config = array(
		'id'           => 'theme_by_socreativ',    		// Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      		// Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', 		// Menu slug.
		'parent_slug'  => 'themes.php',            		// Parent menu slug.
		'capability'   => 'edit_theme_options',    		// Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    		// Show admin notices or not.
		'dismissable'  => false,                   		// If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      		// If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                   		// Automatically activate plugins after installation or not.
	);

	tgmpa( $plugins, $config );

}


add_filter( 'plugin_action_links', 'blacklist_plugins', 10, 4 );
function blacklist_plugins( $actions, $plugin_file, $plugin_data, $context ) {

    if ( array_key_exists( 'activate', $actions ) && in_array( $plugin_file, array(
		'elementor/elementor.php',
		'divi-builder/divi-builder.php',
		'wp-bakery/wp-bakery.php',
		'visualcomposer/plugin-wordpress.php'
	)))
        unset( $actions['activate'] );

    return $actions;
}

function deactive_blacklist_plugin(){

	if(!function_exists('is_plugin_active')){
		require_once ABSPATH . 'wp-admin/includes/plugin.php';
	}


	$blacklist_plugins = array(
		'elementor/elementor.php',
		'divi-builder/divi-builder.php',
		'wp-bakery/wp-bakery.php',
		'visualcomposer/plugin-wordpress.php'
	);

	foreach ($blacklist_plugins as $index => $plugin) {
		if(is_plugin_active($plugin)){
			deactivate_plugins($plugin);
		}
	}

}
deactive_blacklist_plugin();

/*********************************************************************
                            ACF Params
 ********************************************************************/


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' => __('Options'),
		'menu_title' => __('Options'),
		'menu_slug' => 'options',
		'position' => '31'
	));
}

add_action('acf/init', 'my_acf_init_block_types');
function my_acf_init_block_types() {

	/* Add gategorie for blocks */
	function my_block_category($categories, $post){
		return array_merge(
			$categories,
			array(
				array(
					'slug' => 'custom-blocks',
					'title' => __('Custom Block', 'custom-blocks'),
				),
			)
		);
	}
	add_filter('block_categories', 'my_block_category', 10, 2);

	//custom block ACF
	require_once get_template_directory() . '/inc/custom-blocks.php';

}

add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {
	foreach( $items as &$item ) {

		$image = get_field('image', $item);
		// $svg = get_field('svg', $item);
		if( $image ) $item->title = ' <img class="menu-item-icon" src="'.$image['url'].'">';
	}
	return $items;
}

// Import custom walker class
require_once get_template_directory() . '/inc/socreativ-walker.php';

/*********************************************************************
						 CUSTOM FUNCTIONS
 ********************************************************************/

function my_wp_is_mobile() {
    static $is_mobile;

    if ( isset($is_mobile) )
        return $is_mobile;

    if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
        $is_mobile = false;
    } elseif (
        strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false ) {
            $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false && strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') == false) {
            $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
        $is_mobile = false;
    } else {
        $is_mobile = false;
    }

    return $is_mobile;
}


function acf_img($img, $size="large", $class = null, $id = null, $attr = null){
	if(!empty($img)){

		if(isset($img['sizes'][$size])){
			$url = $img['sizes'][$size];
		}

		$srcset = wp_get_attachment_image_srcset($img['id'], $size);
		$sizes = wp_get_attachment_image_sizes($img['id'], $size);
		$alt = $img['alt'];
		$attributes = "";
		foreach ($attr as $k => $v) {
			$attributes .= "$k=\"$v\" ";
		}

		if(function_exists('wp_get_attachment_image_srcset')){
			$img = "<img id=\"$id\" class=\"$class\" src=\"$url\" srcset=\"$srcset\" alt=\"$alt\" sizes=\"$sizes\" $attributes />";
		}

		return $img;

	}
}
